from json import loads, dump
from subprocess import run, PIPE
from argparse import ArgumentParser
from pathlib import Path
from datetime import datetime, timedelta, timezone
from re import compile  as re_compile


def main(path : str, php :str, days: int, output_groups : str, output_quotas : str,
         exclude_group: str):
    output_groups = Path(output_groups)
    output_quotas = Path(output_quotas)

    if days != 0:
        last_seen_limit = datetime.now(timezone.utc)\
            .replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=days)
    else:
        last_seen_limit = None

    if exclude_group:
        exclude_group = re_compile(exclude_group)

    users = {}
    offset = 0
    while True:
        out = run([php, "occ", "user:list", "--info", "--offset", str(offset), "--output", "json"],
                  cwd=path, check=True, stdout=PIPE, stderr=PIPE)
        new_users = loads(out.stdout.decode().strip())
        if not new_users:
            break

        users.update(new_users)
        offset = len(users)

    print(f"Got {offset} users")

    users = {k: v for k, v in users.items() if v["email"]}

    print(f"Got {len(users)} email filtered users")

    if last_seen_limit:
        users = {k: v for k, v in users.items()
                 if datetime.fromisoformat(v["last_seen"]) >= last_seen_limit}

        print(f"Got {len(users)} time filtered users")

    if exclude_group:
        for user in users:
            users[user]["groups"] = list(filter(lambda g: not exclude_group.findall(g),
                                                users[user]["groups"]))
        print("Groups filtered")

    with output_groups.open("w", encoding="UTF-8") as file:
        dump({v["email"]: v["groups"] for (k, v) in users.items()}, file)

    with output_quotas.open("w", encoding="UTF-8") as file:
        dump({v["email"]: v["quota"] for (k, v) in users.items()}, file)


if __name__ == '__main__':
    parser = ArgumentParser(prog="users_groups_quotas_export")
    parser.add_argument("--path", "-p", default="/var/www/nextcloud")
    parser.add_argument("--php", "-P", default="php7.4")
    parser.add_argument("--output-groups", "-g", default="groups.json")
    parser.add_argument("--output-quotas", "-q", default="quotas.json")
    parser.add_argument("--days", "-d", type=int, default="0")
    parser.add_argument("--exclude-group", "-e", default=None)

    args = parser.parse_args()

    main(args.path, args.php, args.days, args.output_groups, args.output_quotas, args.exclude_group)
