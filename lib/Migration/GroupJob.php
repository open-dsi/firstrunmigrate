<?php
/**
 * @copyright Copyright (c) 2016 Joas Schilling <coding@schilljs.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\FirstRunMigrate\Migration;

use OCA\FirstRunMigrate\Migration\MigrationJob;
use OCP\AppFramework\Utility\ITimeFactory;
use OCA\FirstRunMigrate\Migration\Utils;
use OCP\BackgroundJob\IJobList;
use OCP\IConfig;
use OCP\IGroupManager;
use OCP\IUserManager;
use OCP\IUser;
use Psr\Log\LoggerInterface;

class GroupJob extends MigrationJob {
    protected LoggerInterface $logger;

    protected IUserManager $userManager;

    protected IGroupManager $groupManager;

    protected IJobList $jobList;

    public static string $type = 'group';

    public static $next = ShareJob::class;

    /**
     * BackgroundJob constructor.
     *
     * @param INotificationManager $notificationManager
     */
    public function __construct(ITimeFactory $timeFactory, IUserManager $userManager,
        IGroupManager $groupManager, LoggerInterface $logger, IJobList $jobList) {
        parent::__construct($timeFactory);
        $this->logger = $logger;
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
        $this->jobList = $jobList;
    }

    /**
     * @param array $argument
     */
    protected function run($argument) {
        $this->logger->debug("Starting groups migration job {$this->getId()} with args " . json_encode($argument));
        $user = $this->userManager->get($argument['uid']);

        self::setMigrationStatus('started', $user);

        if ($groups = $this->getUserMigration($user)) {
            foreach ($groups as $group) {
                if ($this->groupManager->groupExists($group)) {
                    $group = $this->groupManager->get($group);
                } else {
                    $group = $this->groupManager->createGroup($group);
                }

                $group->addUser($user);
            }
        } else {
            $this->logger->info("{$this->getId()}: No groups to migrate");
        }

        self::setMigrationStatus('finished', $user);

        self::schredule_next($user, $this->logger, $this->jobList);
    }

    private static function getMigrationFile() : ?string {
        /** @var IConfig */
        $config = \OC::$server->get(IConfig::class);

        return $config->getSystemValue('firstrunmigrate_groups', null);
    }

    public static function isMigration() : bool {
        return ($file = self::getMigrationFile()) && file_exists($file);
    }

    private static function getUserMigration(IUser $user) : ?array {
        $groups = json_decode(file_get_contents(self::getMigrationFile()), true);
        $id = Utils::getUserId($user);

        if (array_key_exists($id, $groups)) {
            return $groups[$id];
        } else {
            return null;
        }
    }
}
