<?php
/**
 * @copyright Copyright (c) 2016 Joas Schilling <coding@schilljs.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\FirstRunMigrate\Migration;

use OCA\FirstRunMigrate\Migration\MigrationJob;
use OCP\AppFramework\Utility\ITimeFactory;
use OC_Util;
use OCA\FirstRunMigrate\Migration\Utils;
use OCP\IConfig;
use OCP\IUserManager;
use OCP\IUser;
use OCP\BackgroundJob\IJobList;
use OCP\Files\IRootFolder;
use Psr\Log\LoggerInterface;

class DataJob extends MigrationJob {
    protected LoggerInterface $logger;

    protected IUserManager $userManager;

    protected IJobList $jobList;

    protected IRootFolder $rootFolder;

    public static string $type = 'data';

    public static $next = GroupJob::class;

    /**
     * BackgroundJob constructor.
     *
     * @param INotificationManager $notificationManager
     */
    public function __construct(ITimeFactory $timeFactory, IUserManager $userManager, LoggerInterface $logger,
        IJobList $jobList, IRootFolder $rootFolder) {
        parent::__construct($timeFactory);
        $this->logger = $logger;
        $this->userManager = $userManager;
        $this->jobList = $jobList;
        $this->rootFolder = $rootFolder;
    }

    /**
     * @param array $argument
     */
    protected function run($argument) {
        $this->logger->debug("Starting datas migration job {$this->getId()} with args " . json_encode($argument));
        $uid = $argument['uid'];
        $user = $this->userManager->get($uid);

        self::setMigrationStatus('started', $user);

        if ($migrate_dir = $this->getUserMigration($user)) {
            // Trigger creation of user home and /files folder
            $userFolder = $this->rootFolder->getUserFolder($uid);

            $quota = $user->getQuota();
            $user->setQuota('none');
            OC_Util::copyr($migrate_dir, $userFolder);
            $user->setQuota($quota);

            // update the file cache
            $userFolder->getStorage()->getScanner()->scan('', \OC\Files\Cache\Scanner::SCAN_RECURSIVE);
        } else {
            $this->logger->info("{$this->getId()}: No dir to migrate");
        }

        self::setMigrationStatus('finished', $user);

        self::schredule_next($user, $this->logger, $this->jobList);
    }

    private static function getMigrationDir() : ?string {
        /** @var IConfig */
        $config = \OC::$server->get(IConfig::class);

        return $config->getSystemValue('firstrunmigrate_dir', null);
    }

    public static function isMigration() : bool {
        return ($file = self::getMigrationDir()) && file_exists($file);
    }

    private static function getUserMigration(IUser $user) : ?string {
        $dir = self::getMigrationDir() . '/' . Utils::getUserId($user);

        if (file_exists($dir)) {
            return $dir;
        } else {
            return null;
        }
    }
}
