<?php

namespace OCA\FirstRunMigrate\Migration;

use Exception;
use OCP\IConfig;
use OCP\IUser;
use OCA\FirstRunMigrate\AppInfo\Application;
use OCA\FirstRunMigrate\Exceptions\TooManyUserEmail;
use OCP\IUserManager;
use OCP\Notification\IManager as INotificationManager;

class Utils {
    public static function getUserId(IUser $user) : ?string {
        /** @var IConfig */
        $config = \OC::$server->get(IConfig::class);

        return $config->getUserValue($user->getUID(), 'settings', 'email', null);
    }

    /**
     * @throws Exception
     */
    public static function getUserByID(string $id) : ?IUser {
        $userManager = \OC::$server->get(IUserManager::class);
        $users = $userManager->getByEmail($id);

        if (empty($users)) {
            return null;
        }

        if (count($users) > 1) {
            throw new TooManyUserEmail($id);
        }

        return $users[0];
    }
}
