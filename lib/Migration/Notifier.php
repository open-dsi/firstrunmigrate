<?php
/**
 * @copyright Copyright (c) 2016 Joas Schilling <coding@schilljs.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\FirstRunMigrate\Migration;

use OCP\L10N\IFactory;
use OCP\Notification\INotification;
use OCP\Notification\INotifier;
use OCA\FirstRunMigrate\AppInfo\Application;
use OCP\IURLGenerator;
use OCP\Notification\IManager as INotificationManager;

class Notifier implements INotifier {

    /** @var IFactory */
    protected $factory;

    /** @var INotificationManager */
    protected $notificationManager;

    /** @var IURLGenerator */
    protected $url;

    public function __construct(IFactory $factory, INotificationManager $notificationManager,
        IURLGenerator $urlGenerator) {
        $this->factory = $factory;
        $this->notificationManager = $notificationManager;
        $this->url = $urlGenerator;
    }

    /**
     * Identifier of the notifier, only use [a-z0-9_]
     *
     * @return string
     * @since 17.0.0
     */
    public function getID(): string {
        return Application::APP_ID;
    }

    /**
     * Human readable name describing the notifier
     *
     * @return string
     * @since 17.0.0
     */
    public function getName(): string {
        return $this->factory->get($this->getID())->t('First run migrate');
    }

    /**
     * @param INotification $notification
     * @param string $languageCode The code of the language that should be used to prepare the notification
     * @return INotification
     * @throws \InvalidArgumentException When the notification was not prepared by a notifier
     * @since 9.0.0
     */
    public function prepare(INotification $notification, string $languageCode): INotification {
        if ($notification->getApp() !== $this->getID()) {
            // Not my app => throw
            throw new \InvalidArgumentException();
        }

        $l = $this->factory->get($this->getID(), $languageCode);
        $notify_subject = $notification->getSubject();
        $user = $notification->getUser();
        $notify_parameters = $notification->getSubjectParameters();

        $subject = null;
        $message = null;
        switch ($notify_subject) {
            case 'data_scheduled':
                $subject = $l->t('Data migration scheduled');
                $message = $l->t('Your data will be soon available');
                break;
            case 'data_started':
                $subject = $l->t('Data migration started');
                $message = $l->t('Your data will be soon available');
                break;
            case 'data_finished':
                $subject = $l->t('Data migration complete');
                $message = $l->t('Your data are available !');
                break;
            case 'group_scheduled':
                $subject = $l->t('Groups migration scheduled');
                $message = $l->t('Your group folders will be soon available');
                break;
            case 'group_started':
                $subject = $l->t('Groups migration started');
                $message = $l->t('Your group folders will be soon available');
                break;
            case 'group_finished':
                $subject = $l->t('Groups migration complete');
                $message = $l->t('Your group folders are available !');
                break;
            case 'quota_scheduled':
                $subject = $l->t('Quota migration scheduled');
                break;
            case 'quota_started':
                $subject = $l->t('Quota migration started');
                break;
            case 'quota_finished':
                $subject = $l->t('Quota migration complete');
                break;
            case 'share_scheduled':
                $subject = $l->t('Shares migration scheduled');
                break;
            case 'share_started':
                $subject = $l->t('Shares migration started');
                break;
            case 'share_finished':
                $subject = $l->t('Shares migration complete');
                $message = $l->t('%s shares migrated, %s shares waiting for users, %s missing files, %s errors',
                $notify_parameters);
                break;
            default:
                // Unknown subject => Unknown notification => throw
                throw new \InvalidArgumentException();
        }

        $icon = null;
        $old_notification = $this->notificationManager->createNotification()
            ->setApp(Application::APP_ID)->setUser($user);
        switch ($notification->getObjectId()) {
            case 'scheduled':
                $icon = $this->url->getAbsoluteURL($this->url->imagePath('core', 'actions/recent.svg'));
                break;
            case 'started':
                $icon = $this->url->getAbsoluteURL($this->url->imagePath('core', 'actions/change.svg'));
                $old_notification->setObject($notification->getObjectType(), 'scheduled');
                break;
            case 'finished':
                $icon = $this->url->getAbsoluteURL($this->url->imagePath('core', 'actions/checkmark.svg'));
                $old_notification->setObject($notification->getObjectType(), 'started');
                break;
            default:
                $icon = $this->url->getAbsoluteURL($this->url->imagePath(Application::APP_ID, 'app.svg'));
        }

        if ($old_notification->getObjectId()) {
            $this->notificationManager->markProcessed($old_notification);
        }

        $notification->setParsedSubject($subject);
        if (!empty($message)) {
            $notification->setParsedMessage($message);
        }
        $notification->setIcon($icon);
        return $notification;
    }
}
