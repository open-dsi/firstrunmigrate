<?php

namespace OCA\FirstRunMigrate\Migration;

use OC\BackgroundJob\JobList;
use OCA\FirstRunMigrate\AppInfo\Application;
use OCA\FirstRunMigrate\Migration\Utils;
use OCP\BackgroundJob\QueuedJob;
use OCP\IConfig;
use OCP\IUser;
use Psr\Log\LoggerInterface;
use OCP\Notification\IManager as INotificationManager;


abstract class MigrationJob extends QueuedJob {
    public static string $type;
    public static $next = null;

    public static abstract function isMigration() : bool;

    public static function schredule(IUser $user, LoggerInterface $logger, JobList $jobList) {
        $uid = $user->getUID();
        if (static::isMigration()) {
            $logger->debug("Scheduling $uid " . static::class);
            $jobList->add(static::class, ['uid' => $uid]);
            static::setMigrationStatus('scheduled', $user);
            return true;
        } else {
            $logger->info("$uid don't have " . static::class);
            return static::schredule_next($user, $logger, $jobList);
        }
    }

    public static function schredule_next(IUser $user, LoggerInterface $logger, JobList $jobList) : bool {
        if (static::$next) {
            return static::$next::schredule($user, $logger, $jobList);
        } else {
            return false;
        }
    }

    public static function setMigrationStatus(string $status, IUser $user, array $parameters = array()) {
        /** @var INotificationManager */
        $notificationManager = \OC::$server->get(INotificationManager::class);
        /** @var IConfig */
        $config = \OC::$server->get(IConfig::class);
        $uid = $user->getUID();

        $config->setUserValue($uid, Application::APP_ID, static::$type . '_status', $status);
        $config->setUserValue($uid, Application::APP_ID, static::$type . '_' . $status . '_date',
            (new \DateTime())->getTimestamp());

        $notification = $notificationManager->createNotification();
        $notification->setApp(Application::APP_ID)
            ->setUser($uid)
            ->setDateTime(new \DateTime())
            ->setObject(static::$type, $status)
            ->setSubject(static::$type . '_' .$status, $parameters);
        $notificationManager->notify($notification);
    }

    public static function isUserMigrated(IUser $user) : bool {
        // If user never logged into Nextcloud
        if ($user->getLastLogin() == 0) {
            return false;
        }

        $config = \OC::$server->get(IConfig::class);

        // If user finish the share migration
        return $config->getUserValue($user->getUID(), Application::APP_ID, static::$type . '_status', null) == 'finished';
    }
}
