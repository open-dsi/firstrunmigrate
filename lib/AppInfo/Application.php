<?php
/**
 * @copyright Copyright (c) 2016, Joas Schilling <coding@schilljs.com>
 *
 * @author Joas Schilling <coding@schilljs.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\FirstRunMigrate\AppInfo;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\EventDispatcher\IEventDispatcher;
use OCP\User\Events\UserFirstTimeLoggedInEvent;
use Psr\Log\LoggerInterface;
use OCP\BackgroundJob\IJobList;
use OCA\FirstRunMigrate\Migration\Notifier;
use OCA\FirstRunMigrate\Migration\QuotaJob;
use OCA\FirstRunMigrate\Migration\Utils;

class Application extends App implements IBootstrap {
    public const APP_ID = 'firstrunmigrate';

    public function __construct() {
        parent::__construct('firstrunmigrate');
    }

    public function boot(IBootContext $context): void {
        $context->injectFn([$this, 'registerHooks']);
    }

    public function register(IRegistrationContext $context): void {
        $context->registerNotifierService(Notifier::class);
    }

    public function registerHooks(IEventDispatcher $dispatcher, IJobList $jobList, LoggerInterface $logger) {
        // first time login event setup
        $dispatcher->addListener(UserFirstTimeLoggedInEvent::class,
            function (UserFirstTimeLoggedInEvent $event) use ($jobList, $logger) {
            $logger->debug("Trigger of::firstLogin");

            $user = $event->getUser();
            $uid = $user->getUID();

            $logger->debug("Subject $uid set, checking migration ID");
            // If the user dont have an ID, skip all the migrations
            if (Utils::getUserId($user) === null) {
                $logger->info("$uid don't have a migration ID");
                return;
            }

            QuotaJob::schredule($user, $logger, $jobList);
        });
    }
}
