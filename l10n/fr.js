OC.L10N.register(
    "firstrunwizard",
    {
        "First run migrate": "First run migrate",
        "Data migration scheduled": "Migration des données planifiée",
        "Your data will be soon available": "Vos données seront bientôt disponibles",
        "Data migration started": "La migration des données a commencé",
        "Data migration complete": "Migration des données terminée",
        "Your data are available !": "Vos données sont disponibles !",
        "Groups migration scheduled": "Migration des groupes planifiée",
        "Your group folders will be soon available": "Vos dossiers de groupe seront bientôt disponibles",
        "Groups migration started": "La migration des groupes a commencé",
        "Groups migration complete": "Migration des groupes terminée",
        "Your group folders are available !": "Vos dossiers de groupe sont disponibles !",
        "Quota migration scheduled": "Migration de quota planifiée",
        "Quota migration started": "La migration de quota a commencé",
        "Quota migration complete": "Migration de quota terminée",
        "Shares migration scheduled": "Migration des partages planifiée",
        "Shares migration started": "Vos partages seront bientôt disponibles",
        "Shares migration complete": "Migration des partages terminée",
        "%s shares migrated, %s shares waiting for users, %s missing files, %s errors": "%s partages migrés, %s partages en attente d'utilisateurs, %s fichiers manquants, %s erreurs"
},
"nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;");
